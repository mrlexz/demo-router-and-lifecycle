import React from 'react';

class Clock extends React.Component {
    state = {
        // clock: [
        //     {
        //         hour: new Date().getHours,
        //         minute: new Date().getMinutes,
        //         second: new Date().getSeconds
        //     }
        // ],
        isNight: '',
        date: new Date()

    }
    // timeCounter = () => {
    //     let d = new Date();
    //     let h = d.getHours();
    //     let m = d.getMinutes();
    //     let s = d.getSeconds();
    //     let flag = this.state.isNight;
    //     s++;
    //     if (s === 60) {
    //         m++;
    //         s = 0;
    //     }
    //     if (m === 60) {
    //         h++;
    //         m = 0;
    //     }
    //     if (h === 24) {
    //         h = 0;
    //     }
    //     if (d.getHours <= 12) {
    //         flag = 'AM'
    //     } else {
    //         flag = 'PM'
    //     }
    //     // d.setHours(2);
    //     this.setState({
    //         hour: h,
    //         minute: m,
    //         second: s,
    //         isNight: flag
    //     })
    //     console.log(this.state.clock);
    // }
    tick = () => {
        this.setState({
            date: new Date()
        })
    }
    componentDidMount() {
        // this.timeID = setInterval(() => this.timeCounter(), 1000)
        this.timeID = setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timeID);
        console.log('end');
    }

    render() {
        return (
            <div>
                <h1 >Clock</h1>
                {/* <h3>Time: {this.state.hour}:{this.state.minute}:{this.state.second} {this.state.isNight}</h3> */}
                <h3>Time: {this.state.date.toLocaleTimeString()}</h3>
            </div>
        )
    }
}
export default Clock;